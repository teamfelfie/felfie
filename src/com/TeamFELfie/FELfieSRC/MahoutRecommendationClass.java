package com.TeamFELfie.FELfieSRC;

import java.io.File;
import java.io.IOException;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;

public class MahoutRecommendationClass {
	private DataModel model;

	public MahoutRecommendationClass(String locationToCSV) {
		try {
			model = new FileDataModel(new File(locationToCSV));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			UserSimilarity userSim = new PearsonCorrelationSimilarity(model);
			UserNeighborhood neighbours = new ThresholdUserNeighborhood(0.1, userSim, model);
			UserBasedRecommender userRecommender = new GenericUserBasedRecommender(model, neighbours, userSim);
			//eka parametri, lue k�ytt�j�n lokaalista csv fillusta k�ytt�j�n id, toka parametri = annettavien suositusten m��r�
			List recommendations = userRecommender.recommend(arg0, arg1);
			
		} catch (TasteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
