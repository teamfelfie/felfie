package com.TeamFELfie.FELfieSRC;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import net.miginfocom.swing.MigLayout;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JTree;

public class ChatRoomWindow {

	private JFrame frame;
	/**
	 * @wbp.nonvisual location=653,189
	 */
	private final JTree tree = new JTree();
	private JList list_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatRoomWindow window = new ChatRoomWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ChatRoomWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 539, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new MigLayout("", "[][][][][][][][][][][][][][][][][grow]", "[grow]"));
		
		JList list = new JList();
		frame.getContentPane().add(list, "flowx,cell 15 0 2 1,growx,aligny top");
		
		list_1 = new JList();
		frame.getContentPane().add(list_1, "cell 16 0,grow");
	}

}
