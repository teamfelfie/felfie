package com.TeamFELfie.FELfieSRC;

import java.awt.EventQueue;

import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class ClientFirstGUI {

	private final class EiDiggaaButtonActionObject implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
		
		}
	}

	private final class DiggaaButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			
		}
	}

	private JFrame frmFelfie;
	private JTextField textField;
	private JLabel lblNewLabel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//T�ss� on vaan testauskommentti
					ClientFirstGUI window = new ClientFirstGUI();
					window.frmFelfie.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ClientFirstGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFelfie = new JFrame();
		frmFelfie.setTitle("FELFIE");
		frmFelfie.setBounds(100, 100, 547, 468);
		frmFelfie.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFelfie.getContentPane().setLayout(new MigLayout("", "[][][][][][][][][][][][][][][][][][][][][][][][]", "[][][][][][][][][][][][][][][][][][]"));
		
		textField = new JTextField();
		textField.setEditable(false);
		frmFelfie.getContentPane().add(textField, "cell 1 0 23 9,grow");
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Diggaa");
		btnNewButton.addActionListener(new DiggaaButtonActionListener());
		
		lblNewLabel = new JLabel("RuokaX");
		frmFelfie.getContentPane().add(lblNewLabel, "cell 0 9 24 1,alignx center");
		frmFelfie.getContentPane().add(btnNewButton, "cell 3 15");
		
		JButton btnNewButton_1 = new JButton("Ei Diggaa");
		btnNewButton_1.addActionListener(new EiDiggaaButtonActionObject());
		frmFelfie.getContentPane().add(btnNewButton_1, "cell 19 15");
	}

}
